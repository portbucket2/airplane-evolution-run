T-Pose					: 0-5

Idle Animation				: 580-804

Run
-------
	- One Year			: 10-38
	- 5 year			: 340-359
	- 10-15 year			: 310-334
	- 20-30 year			: 40-57
	- 40 year - with briefcase	: 430-449
	- 60 year			: 400-420
	- 80 year - with stick		: 370-398
	- 100 year- wheel chair/Idle	: 155-257
Push
------
	- One year			: 460-488
	- 40 year & 80 year		: 500-564
	- 5-30 year & 60 year		: 60-124
	- 100 year			: 270-305
Stair climbing
-----------------
	- one year			: None
	- 5-80 year			: 130-152
	- 100 year			: None

Note : Brief case and Stick models are under Right hand bone.