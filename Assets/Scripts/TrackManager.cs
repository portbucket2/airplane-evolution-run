using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TrackManager : APBehaviour
{
    public Transform startPoint;
    public Transform endPoint;
    public Transform waypointsHolder;

    #region ALL UNITY FUNCTIONS

    // Awake is called before Start
    public override void Awake()
    {
        base.Awake();
       
    }
    

    // Start is called before the first frame update
    public override void Start()
    {
        base.Start();
    }

    void Update()
    {
        //if (gameState.Equals(GameState.GAME_INITIALIZED) && Input.GetMouseButtonDown(0))
        //{
        //    gameManager.ChangeGameState(GameState.GAME_PLAY_STARTED);
        //    gameState = GameState.GAME_PLAY_STARTED;
        //}

        if (!gameState.Equals(GameState.GAME_PLAY_STARTED))
            return;

    }

    void FixedUpdate()
    {
        if (!gameState.Equals(GameState.GAME_PLAY_STARTED))
            return;

    }

    void LateUpdate()
    {
        if (!gameState.Equals(GameState.GAME_PLAY_STARTED))
            return;

    }

    #endregion ALL UNITY FUNCTIONS
    //=================================   
    #region ALL OVERRIDING FUNCTIONS

    public override void OnGameInitializing()
    {
        base.OnGameInitializing();
        startPoint = transform.GetChild(1);
        endPoint = transform.GetChild(0);
        waypointsHolder = transform.GetChild(3);
    }
    #endregion ALL OVERRIDING FUNCTIONS
    //=================================
    #region ALL SELF DECLEAR FUNCTIONS

    public void OnEditor()
    {
        startPoint = transform.GetChild(1);
        endPoint = transform.GetChild(0);
        waypointsHolder = transform.GetChild(3);
    }
    #endregion ALL SELF DECLEAR FUNCTIONS

}
