using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public class ObstacleDataDistributor : APBehaviour
{
    public AssetDataSO obstacleData;
    private AssetDataSO tempValue;
    
    private readonly string assetPath = "Assets/Scriptable Object/";

    #region ALL UNITY FUNCTIONS

    // Awake is called before Start
    public override void Awake()
    {
        base.Awake();
    }

    // Start is called before the first frame update
    public override void Start()
    {
        base.Start();
    }

    void Update()
    {
        //if (gameState.Equals(GameState.GAME_INITIALIZED) && Input.GetMouseButtonDown(0))
        //{
        //    gameManager.ChangeGameState(GameState.GAME_PLAY_STARTED);
        //    gameState = GameState.GAME_PLAY_STARTED;
        //}

        if (!gameState.Equals(GameState.GAME_PLAY_STARTED))
            return;

    }

    void FixedUpdate()
    {
        if (!gameState.Equals(GameState.GAME_PLAY_STARTED))
            return;

    }

    void LateUpdate()
    {
        if (!gameState.Equals(GameState.GAME_PLAY_STARTED))
            return;

    }

    #endregion ALL UNITY FUNCTIONS
    //=================================   
    #region ALL OVERRIDING FUNCTIONS
    public override void OnGameInitializing()
    {
        GetComponent<Obstacle>().Initialize(obstacleData);
    }

    #endregion ALL OVERRIDING FUNCTIONS
    //=================================
    #region ALL SELF DECLEAR FUNCTIONS
    public void Distribute(TrackData trackdata, Dictionary<GameObject, AssetDataSO> dict)
    {
        
        int randomIndex = UnityEngine.Random.Range(0, trackdata.obstaclePrefab.Length);
        GameObject obstacle = Instantiate(trackdata.obstaclePrefab[randomIndex], transform);
        obstacle.name = trackdata.obstaclePrefab[randomIndex].name;
        if(dict.TryGetValue(trackdata.obstaclePrefab[randomIndex], out tempValue))
        {
            obstacleData = tempValue;
        }
        
        
    }
    
    
    #endregion ALL SELF DECLEAR FUNCTIONS

}
