using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class InteractableCollision : APBehaviour
{
    private bool _isCalledOnceOnEnteringArea = false;
    public static Action<string, Collider> OnTriggerEnterAction;
    public static Action<string, Collider> OnTriggerExitAction;
    #region ALL UNITY FUNCTIONS

    // Awake is called before Start
    public override void Awake()
    {
        base.Awake();
    }

    // Start is called before the first frame update
    public override void Start()
    {
        base.Start();
    }

    void Update()
    {
        //if (gameState.Equals(GameState.GAME_INITIALIZED) && Input.GetMouseButtonDown(0))
        //{
        //    gameManager.ChangeGameState(GameState.GAME_PLAY_STARTED);
        //    gameState = GameState.GAME_PLAY_STARTED;
        //}

        if (!gameState.Equals(GameState.GAME_PLAY_STARTED))
            return;

    }

    void FixedUpdate()
    {
        if (!gameState.Equals(GameState.GAME_PLAY_STARTED))
            return;

    }

    void LateUpdate()
    {
        if (!gameState.Equals(GameState.GAME_PLAY_STARTED))
            return;

    }
    private void OnTriggerEnter(Collider other)
    {
        if (!_isCalledOnceOnEnteringArea)
        {
            OnTriggerEnterAction?.Invoke(other.tag, other);
            _isCalledOnceOnEnteringArea = true;
        }
    }
    private void OnTriggerExit(Collider other)
    {
        if (_isCalledOnceOnEnteringArea)
        {
            OnTriggerExitAction?.Invoke(other.tag, other);
            _isCalledOnceOnEnteringArea = false;
        }
    }
    #endregion ALL UNITY FUNCTIONS
    //=================================   
    #region ALL OVERRIDING FUNCTIONS


    #endregion ALL OVERRIDING FUNCTIONS
    //=================================
    #region ALL SELF DECLEAR FUNCTIONS


    #endregion ALL SELF DECLEAR FUNCTIONS

}
