using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

#if UNITY_EDITOR

using UnityEditor;

#endif

public class PlayerController : APBehaviour
{
    #region Public Variables
    public float forwardMovementSpeed;
    [Range(0, 1)]
    public float lerpValueRange;
    public float turnSpeed;
    public float dragSensitivity;
    public float trackDisplacementZ;
    #endregion
    #region Private Variables
    private Vector3 newPosition;
    private Vector3 previousRotation;
    private int previousWaypointIndex = 0;
    private int nextWaypointIndex = 0;
    private GameManager _gameManager;
    private Transform childTransform;
    private Vector3 childTransformPosition;
    private CapsuleCollider selfCollider;
    private bool isCalledOnce = false;
    private bool isMoving = true;
    private Animator _animator;

    private int run = Animator.StringToHash("isGate");
    private int gate = Animator.StringToHash("isGate");
    private int pickup = Animator.StringToHash("isPickup");
    private int obstacle = Animator.StringToHash("isObstacle");
    public float turnValue;
    private bool isGameStarted = false;
    public Transform previousWaypoint;
    public Transform nextWaypoint;

    #endregion
    #region ALL UNITY FUNCTIONS

    // Awake is called before Start
    public override void Awake()
    {
        base.Awake();
        Registar_For_Input_Callback();
    }

    // Start is called before the first frame update
    public override void Start()
    {
        base.Start();
        _animator = transform.GetChild(0).GetComponent<Animator>(); 
    }
    public override void OnEnable()
    {
        base.OnEnable();
       
    }
    public override void OnDisable()
    {
        base.OnDisable();
        
    }

#if UNITY_EDITOR

    private void OnDrawGizmosSelected()
    {
        if (nextWaypoint != null)
        {
            Gizmos.color = Color.green;
            Gizmos.DrawSphere(nextWaypoint.position, 0.5f);

        }

        if (previousWaypoint != null)
        {
            Gizmos.color = Color.red;
            Gizmos.DrawSphere(previousWaypoint.position, 0.3f);
        }

        if (nextWaypoint != null && previousWaypoint != null)
        {

            float totalDistance = Vector3.Distance(nextWaypoint.position, previousWaypoint.position);
            float currentDistance = Vector3.Distance(transform.position, nextWaypoint.position);
            float lerpedValue = 1f - (currentDistance / totalDistance);
            Handles.DrawAAPolyLine(previousWaypoint.position, nextWaypoint.position);

            GUIStyle labelStyle = new GUIStyle(EditorStyles.label);
            labelStyle.normal.textColor = Color.Lerp(Color.red, Color.green, lerpedValue);
            Handles.Label(
                Vector3.Lerp(previousWaypoint.position, nextWaypoint.position, 0.5f) + Vector3.up,
                string.Format("WP({0}) :CurrentDistance = {1}\nWP({2}) : TargetedDistance = {3}\nLerpedValue = {4}",
                previousWaypointIndex,
                currentDistance,
                nextWaypointIndex,
                totalDistance,
                lerpedValue),
                labelStyle);

        }
    }

#endif



    void Update()
    {
        if (gameState.Equals(GameState.GAME_INITIALIZED) && isGameStarted)
        {
            gameManager.ChangeGameState(GameState.GAME_PLAY_STARTED);
            gameState = GameState.GAME_PLAY_STARTED;
            _animator.SetTrigger(run);
        }
        if (!gameState.Equals(GameState.GAME_PLAY_STARTED))
            return;     


    }

    void FixedUpdate()
    {
        if (!gameState.Equals(GameState.GAME_PLAY_STARTED))
            return;
        if(nextWaypointIndex < _gameManager.trackManager.waypointsHolder.childCount - 1)
        {
            ConstantForwardMovement();
            XAxisMovement();
            WayPointCalculation();
            float distance = Vector3.Distance(transform.position, nextWaypoint.position) /
                         Vector3.Distance(previousWaypoint.position, nextWaypoint.position);
            turnValue = (1 - distance) * turnSpeed;
            transform.rotation = Quaternion.Lerp(previousWaypoint.rotation, nextWaypoint.rotation, turnValue);


        }
        else
        {
            transform.eulerAngles = Vector3.zero;
            if(isMoving)
            {
                XAxisMovement();
                ConstantForwardMovement();
            }
                

            if (!isCalledOnce)
            {
                CurrencyDataManager.Instance.StartCountdown(true);
                isCalledOnce = true;
            }
            
        }
        selfCollider.center = new Vector3(transform.GetChild(0).localPosition.x, selfCollider.center.y, selfCollider.center.z);
    }

    

    void LateUpdate()
    {
        if (!gameState.Equals(GameState.GAME_PLAY_STARTED))
            return;

    }
    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.layer.Equals(ConstantManager.GATE_LAYER))
        {
            other.GetComponent<Gate>().Execute();
            _animator.SetTrigger(gate);
            
        }
        else if(other.gameObject.layer.Equals(ConstantManager.ENEMY_LAYER))
        {
            other.GetComponent<Obstacle>().Execute();
            _animator.SetTrigger(obstacle);
        }
        else if(other.gameObject.layer.Equals(ConstantManager.PICKUPS_LAYER))
        {
            other.GetComponent<Pickup>().Execute();
            _animator.SetTrigger(pickup);
        }
    }
    #endregion ALL UNITY FUNCTIONS
    //=================================   
    #region ALL OVERRIDING FUNCTIONS

    public override void OnDrag(Vector3 dragAmount)
    {
        if (gameState.Equals(GameState.GAME_INITIALIZED))
            isGameStarted = true;

        if (!gameState.Equals(GameState.GAME_PLAY_STARTED))
            return;
        base.OnDrag(dragAmount);
        childTransformPosition.x = Mathf.Clamp(childTransformPosition.x + dragAmount.x * Time.deltaTime * dragSensitivity, -trackDisplacementZ, trackDisplacementZ);
    }
    public override void OnTapStart(Vector3 tapOnWorldSpace)
    {
        base.OnTapStart(tapOnWorldSpace);
    }
    public override void OnGameStart()
    {
        base.OnGameStart();
    }
    public override void OnGameInitializing()
    {
        base.OnGameInitializing();
        _gameManager = (GameManager)gameManager;
        childTransform = transform.GetChild(0);
        selfCollider = GetComponent<CapsuleCollider>();
        previousWaypointIndex = 0;
        nextWaypointIndex = 0;
        CurrencyDataManager.OnCountFinish += GameOver;
    }



    #endregion ALL OVERRIDING FUNCTIONS
    //=================================
    #region ALL SELF DECLEAR FUNCTIONS

    private void WayPointCalculation()
    {
        nextWaypointIndex = NextWaypointProvider(nextWaypointIndex);
        previousWaypointIndex = nextWaypointIndex != 0 ? nextWaypointIndex - 1 : previousWaypointIndex;
        previousWaypoint = _gameManager.trackManager.waypointsHolder.GetChild(previousWaypointIndex);
        //Debug.Log("<color=green> Previous Waypoint </color>" + previousWaypointIndex);
        nextWaypoint = _gameManager.trackManager.waypointsHolder.GetChild(nextWaypointIndex);
        //Debug.Log("<color=yellow> Next Waypoint </color>" + nextWaypointIndex);
        
    }
    private void ConstantForwardMovement()
    {
        transform.position += transform.forward * forwardMovementSpeed * Time.fixedDeltaTime;
    }
    private void XAxisMovement()
    {
        childTransformPosition.y = 0;
        childTransformPosition.z = 0;
        transform.GetChild(0).localPosition = Vector3.Lerp(transform.GetChild(0).localPosition, childTransformPosition, lerpValueRange);
    }

    private int NextWaypointProvider(int currentWaypoint)
    {
        Transform current = _gameManager.trackManager.waypointsHolder.GetChild((int)currentWaypoint);
        Vector3 distanceVector = transform.position - current.position;
        if (Vector3.Dot(distanceVector, current.forward) < 0)
        {
            return currentWaypoint;
        }
        else
        {
            return NextWaypointProvider(currentWaypoint + 1);
        }
    }
    private void GameOver(GameState state)
    {
        if (state == GameState.GAME_PLAY_ENDED)
        {
            isMoving = false;
            if(gameState.Equals(GameState.GAME_PLAY_STARTED))
                gameManager.ChangeGameState(state);

        }

    }
    
    #endregion ALL SELF DECLEAR FUNCTIONS

}

//Vector3 newRotation = _gameManager.trackManager.waypointsHolder.GetChild(nextWaypointIndex).position - transform.position;
//Vector3 relative = newRotation;
//float angle = Mathf.Atan2(relative.x, relative.z) * Mathf.Rad2Deg;
//Vector3 nextRotation = new Vector3(0, angle, 0);