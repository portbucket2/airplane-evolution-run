using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PickupDataDistributor : APBehaviour
{
    public AssetDataSO pickupData;
    private AssetDataSO tempValue;
    #region ALL UNITY FUNCTIONS

    // Awake is called before Start
    public override void Awake()
    {
        base.Awake();
    }

    // Start is called before the first frame update
    public override void Start()
    {
        base.Start();
    }

    void Update()
    {
        //if (gameState.Equals(GameState.GAME_INITIALIZED) && Input.GetMouseButtonDown(0))
        //{
        //    gameManager.ChangeGameState(GameState.GAME_PLAY_STARTED);
        //    gameState = GameState.GAME_PLAY_STARTED;
        //}

        if (!gameState.Equals(GameState.GAME_PLAY_STARTED))
            return;

    }

    void FixedUpdate()
    {
        if (!gameState.Equals(GameState.GAME_PLAY_STARTED))
            return;

    }

    void LateUpdate()
    {
        if (!gameState.Equals(GameState.GAME_PLAY_STARTED))
            return;

    }

    #endregion ALL UNITY FUNCTIONS
    //=================================   
    #region ALL OVERRIDING FUNCTIONS

    public override void OnGameInitializing()
    {
        GetComponent<Pickup>().Initialize(pickupData);
    }
    #endregion ALL OVERRIDING FUNCTIONS
    //=================================
    #region ALL SELF DECLEAR FUNCTIONS
    public void Distribute(TrackData trackdata, Dictionary<GameObject, AssetDataSO> dict)
    {

        int randomIndex = UnityEngine.Random.Range(0, trackdata.pickupPrefab.Length);
        GameObject pickup = Instantiate(trackdata.pickupPrefab[randomIndex], transform);
        pickup.name = trackdata.pickupPrefab[randomIndex].name;
        if (dict.TryGetValue(trackdata.pickupPrefab[randomIndex], out tempValue))
        {
            pickupData = tempValue;
        }
        

    }

    #endregion ALL SELF DECLEAR FUNCTIONS

}
