using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using TMPro;
using UnityEngine.UI;



public class CurrencyFormatter : ICustomFormatter, IFormatProvider
{
    public object GetFormat(Type formatType)
    {
        return (formatType == typeof(ICustomFormatter)) ? this : null;
    }

    public string Format(string format, object arg, IFormatProvider formatProvider)
    {
        if (format == null || !format.Trim().StartsWith("K"))
        {
            if (arg is IFormattable)
            {
                return ((IFormattable)arg).ToString(format, formatProvider);
            }
            return arg.ToString();
        }

        decimal value = Convert.ToDecimal(arg);

        //  Here's is where you format your number

        if (value > 1000)
        {
            return (value / 1000).ToString() + "k";
        }

        return value.ToString();
    }
}
public class CurrencyDataManager : APBehaviour
{
    public static CurrencyDataManager Instance;
    #region Private Variables
    private float yearRemaining;
    private bool isTimerRunning = false;
    

    #endregion
    #region Public Variables
    public TMPro.TextMeshProUGUI textReference;
    public Action<int> OnAgeChange;
    public Action<int> OnCoinChange;
    public static Action<GameState> OnCountFinish;
    public delegate MeshFabricator.AgeActor MeshChangeDelegate(int age);
    public static event MeshChangeDelegate MeshShouldChangeEvent;

    public UpgradeEntity ageUpgradeEntity;
    public UpgradeEntity incomeUpgradeEntity;
    public TMPro.TextMeshProUGUI gameScoreText;
    [Header("Age Upgrade Data")]
    public TMPro.TextMeshProUGUI ageLevelText;
    public TMPro.TextMeshProUGUI ageCostText;
    public Button ageUpgradeButton;
    [Header("Income Upgrade Data")]
    public TMPro.TextMeshProUGUI incomeLevelText;
    public TMPro.TextMeshProUGUI incomeCostText;
    public Button incomeUpgradeButton;
    #endregion
    protected DateTime age;
    public DateTime Age { get => age; set => age = value; }

    private const int baseYear = 1980;

   
    #region ALL UNITY FUNCTIONS

    // Awake is called before Start
    public override void Awake()
    {
        base.Awake();
        if (Instance != null)
        {
            Destroy(gameObject);
            return;
        }

        Instance = this;
        DontDestroyOnLoad(gameObject);
        Input.backButtonLeavesApp = true;
    }
    public override void OnEnable()
    {
        base.OnEnable();
        OnAgeChange += UpdateDate;
        OnCoinChange += UpdateGameScore;
    }

    

    public override void OnDisable()
    {
        base.OnDisable();
        OnAgeChange -= UpdateDate;
        OnCoinChange -= UpdateGameScore;
    }
    

    // Start is called before the first frame update
    public override void Start()
    {
        base.Start();
        ageUpgradeEntity = new UpgradeEntity(gameplayData.upgradeRate, 
                                             gameplayData.ageUpgradeCost, 
                                             gameplayData.ageUpgradeLevel, false);
        incomeUpgradeEntity = new UpgradeEntity(gameplayData.upgradeRate, 
                                                gameplayData.incomeUpgradeCost, 
                                                gameplayData.incomeUpgradeLevel, false);


    }
    private void UpdateDate(int obj)
    {
        textReference.text = obj.ToString();
        yearRemaining = obj;
        
    }
    void Update()
    {
        //if (gameState.Equals(GameState.GAME_INITIALIZED) && Input.GetMouseButtonDown(0))
        //{
        //    gameManager.ChangeGameState(GameState.GAME_PLAY_STARTED);
        //    gameState = GameState.GAME_PLAY_STARTED;
        //}

        if (!gameState.Equals(GameState.GAME_PLAY_STARTED))
            return;
        Countdown();

    }

    void FixedUpdate()
    {
        if (!gameState.Equals(GameState.GAME_PLAY_STARTED))
            return;

    }

    void LateUpdate()
    {
        if (!gameState.Equals(GameState.GAME_PLAY_STARTED))
            return;

    }

    #endregion ALL UNITY FUNCTIONS
    //=================================   
    #region ALL OVERRIDING FUNCTIONS
    public override void OnGameDataLoad()
    {
        base.OnGameDataLoad();
        ReferenceIntitialize();
        UpgradeDatatLoad();
    }

    private void ReferenceIntitialize()
    {
        textReference = gameManager.gameCustomUI.transform.GetChild(2).GetChild(1).GetComponent<TextMeshProUGUI>();
        gameScoreText = gameManager.gameCustomUI.transform.GetChild(2).GetChild(2).GetComponent<TextMeshProUGUI>();
        ageLevelText = gameManager.gameStartingUI.transform.GetChild(3).GetChild(0).GetChild(0).GetChild(1).GetComponent<TextMeshProUGUI>();
        ageCostText = gameManager.gameStartingUI.transform.GetChild(3).GetChild(0).GetChild(2).GetChild(0).GetComponent<TextMeshProUGUI>();
        ageUpgradeButton = gameManager.gameStartingUI.transform.GetChild(3).GetChild(0).GetComponent<Button>();
        incomeLevelText = gameManager.gameStartingUI.transform.GetChild(3).GetChild(1).GetChild(0).GetChild(1).GetComponent<TextMeshProUGUI>();
        incomeCostText = gameManager.gameStartingUI.transform.GetChild(3).GetChild(1).GetChild(2).GetChild(0).GetComponent<TextMeshProUGUI>();
        incomeUpgradeButton = gameManager.gameStartingUI.transform.GetChild(3).GetChild(0).GetComponent<Button>();
    }

    public override void OnGameInitializing()
    {
        base.OnGameInitializing();
        
       
    }
    
    #endregion ALL OVERRIDING FUNCTIONS
    //=================================
    #region ALL SELF DECLEAR FUNCTIONS
    public void Init()
    {
        Age = new DateTime(baseYear, 1, 1);
        textReference.text = Age.Year.ToString();
        
    }
    private void UpdateGameScore(int obj)
    {
        gameScoreText.text = obj.ToString();
    }
    public int GetDays(DateType dateType, int number)
    {
        return dateType switch
        {
            DateType.YEAR => 365 * number,
            DateType.MONTH => 30 * number,
            DateType.WEEK => 7 * number,
            DateType.DAY => number,
            _ => number,
        };
    }
    public void ApplyOperator(GateValue value)
    {
        switch (value.operatorType)
        {
            case Operator.ADD:
                Age = Age.AddDays(GetDays(value.dateType, value.value));
                break;
            case Operator.SUBTRACT:
                Age = Age.AddDays(-GetDays(value.dateType, value.value));
                break;
            case Operator.MULTIPLY:
                Age = Age.AddYears(Age.Year * (value.value - 1));
                break;
            case Operator.DIVIDE:
                Age.AddDays(-GetDays(DateType.YEAR, value.value));
                break;
            default:
                break;
        }
        
        OnAgeChange?.Invoke(Age.Year);
        

    }
    private void UpgradeDatatLoad()
    {
        UITextUpdate();
        IsAgeUpgradable();
        IsIncomeUpgradable();
        ageUpgradeButton.onClick.AddListener(AgeUpgradeCalculation);
        incomeUpgradeButton.onClick.AddListener(IncomeUpgradeCalculation);

    }
    private void UITextUpdate()
    {
        gameScoreText.text = gameplayData.gameScore.ToString();
        ageCostText.text = gameplayData.ageUpgradeCost.ToString();
        ageLevelText.text = gameplayData.ageUpgradeLevel.ToString();
        incomeLevelText.text = gameplayData.incomeUpgradeLevel.ToString();
        incomeCostText.text = gameplayData.incomeUpgradeCost.ToString();
        
    }
    private bool IsAgeUpgradable()
    {     
        return ageUpgradeButton.interactable = ageUpgradeEntity.upgradeCost < gameplayData.gameScore ?
               true : false;
    }
    private bool IsIncomeUpgradable()
    {
        return incomeUpgradeButton.interactable = incomeUpgradeEntity.upgradeCost < gameplayData.gameScore ?
               true : false;
    }
    public void CalculateAgeUpgrade()
    {
        if(IsAgeUpgradable())
        {
            
            gameplayData.gameScore -= gameplayData.ageUpgradeCost;
            Age = Age.AddYears(Mathf.CeilToInt(ageUpgradeEntity.rate * (5 * ageUpgradeEntity.levelCount)));
            OnAgeChange?.Invoke(Age.Year);

            ageUpgradeEntity.levelCount++;
            gameplayData.ageUpgradeLevel = ageUpgradeEntity.levelCount;

            ageUpgradeEntity.upgradeCost += Mathf.CeilToInt(ageUpgradeEntity.rate * ageUpgradeEntity.upgradeCost);
            gameplayData.ageUpgradeCost = ageUpgradeEntity.upgradeCost;
            gameManager.SaveGame();
            IsAgeUpgradable();
        }
       
    }
    
    public void StartCountdown(bool activate)
    {
        isTimerRunning = activate;
    }
    public void Countdown()
    {
        if(isTimerRunning)
        {
            if(yearRemaining > 0)
            {
                yearRemaining -= Time.deltaTime;
                
                OnAgeChange?.Invoke(Mathf.FloorToInt(yearRemaining));
            }
            else
            {
                yearRemaining = 0;
                isTimerRunning = false;
                OnAgeChange?.Invoke(Mathf.FloorToInt(yearRemaining));
                OnCountFinish?.Invoke(GameState.GAME_PLAY_ENDED);
            }
        }
    }
    
    private void IncomeUpgradeCalculation()
    {
        if(IsIncomeUpgradable())
        {
            gameplayData.gameScore -= gameplayData.incomeUpgradeCost;

            incomeUpgradeEntity.levelCount++;
            gameplayData.incomeUpgradeLevel = incomeUpgradeEntity.levelCount;

            incomeUpgradeEntity.upgradeCost += Mathf.CeilToInt(incomeUpgradeEntity.rate * incomeUpgradeEntity.upgradeCost);
            gameplayData.incomeUpgradeCost = incomeUpgradeEntity.upgradeCost;
            gameManager.SaveGame();
            IsIncomeUpgradable();
        }
        UITextUpdate();
    }

    private void AgeUpgradeCalculation()
    {
        CalculateAgeUpgrade();
        UITextUpdate();
    }

    public void ReductAge(int value)
    {
        Age = Age.AddYears(-value);
        OnAgeChange?.Invoke(Age.Year);
        
    }

    public void PickupEffect(int value)
    {
        gameplayData.gameScore += value;
        gameManager.SaveGame();
        OnCoinChange?.Invoke(gameplayData.gameScore);
    }
    #endregion ALL SELF DECLEAR FUNCTIONS

}
