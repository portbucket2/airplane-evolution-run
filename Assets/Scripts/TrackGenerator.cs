using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;


public class TrackGenerator : APBehaviour
{
    public TrackData trackData;
    public Transform playerPrefab;
    [RangeExtension(10f, 50f, 5)]
    public float distanceDelta;

    public Dictionary<GameObject, AssetDataSO> assetLibrary = new Dictionary<GameObject, AssetDataSO>();
    #region Private Variables
    private GameManager _gameManager;
    private GameObject previousGate;
    private readonly string assetPath = "Assets/Scriptable Object/";
    private float obstacleXOffset = -1f;
    #endregion
    #region ALL UNITY FUNCTIONS

    // Awake is called before Start
    public override void Awake()
    {
        base.Awake();
    }

    // Start is called before the first frame update
    public override void Start()
    {
        base.Start();
    }

    void Update()
    {
        //if (gameState.Equals(GameState.GAME_INITIALIZED) && Input.GetMouseButtonDown(0))
        //{
        //    gameManager.ChangeGameState(GameState.GAME_PLAY_STARTED);
        //    gameState = GameState.GAME_PLAY_STARTED;
        //}

        if (!gameState.Equals(GameState.GAME_PLAY_STARTED))
            return;

    }

    void FixedUpdate()
    {
        if (!gameState.Equals(GameState.GAME_PLAY_STARTED))
            return;

    }

    void LateUpdate()
    {
        if (!gameState.Equals(GameState.GAME_PLAY_STARTED))
            return;

    }



    #endregion ALL UNITY FUNCTIONS
    //=================================   
    #region ALL OVERRIDING FUNCTIONS

    public override void OnGameInitializing()
    {
        base.OnGameInitializing();
        _gameManager = (GameManager)gameManager;
    }
    #endregion ALL OVERRIDING FUNCTIONS
    //=================================
    #region ALL SELF DECLEAR FUNCTIONS

    public void GenerateTrack()
    {
        PrepareBeforeDistribute();
        for (int i = transform.childCount - 1; i > -1; i--)
        {
            GameObject.DestroyImmediate(transform.GetChild(i).gameObject);
        }
        GameObject track = Instantiate(trackData.trackMesh, Vector3.zero, Quaternion.identity, transform);
        GameObject player = Instantiate(playerPrefab.gameObject, Vector3.zero, Quaternion.identity, transform);
        player.transform.GetChild(0).gameObject.layer = LayerMask.NameToLayer("PLAYER_LAYER");
        TrackManager spawnedTrack = track.AddComponent<TrackManager>();
        spawnedTrack.OnEditor();
        for(int i = 0; i < spawnedTrack.waypointsHolder.childCount; i++)
        {
            //Vector3 position = new Vector3(0, 0, trackData.interval * i);
            if(previousGate != null)
            {
                float distance = spawnedTrack.waypointsHolder.GetChild(i).position.z - previousGate.transform.position.z + trackData.interval;
                Transform closestGatePoint = distance >= distanceDelta ?
                                             spawnedTrack.waypointsHolder.GetChild(i) : null;
                Transform closestObstaclePoint = distance >= distanceDelta ?
                                                 spawnedTrack.waypointsHolder.GetChild(i + 1) : null;
                if(closestGatePoint == null)
                {
                    continue;
                }
                else
                {
                    Vector3 g_position = closestGatePoint.position;
                    Quaternion g_rotation = closestGatePoint.rotation;
                    Vector3 o_position = new Vector3(closestObstaclePoint.position.x + obstacleXOffset,
                                                     closestObstaclePoint.position.y,
                                                     closestObstaclePoint.position.z);
                    Vector3 p_position = new Vector3(closestObstaclePoint.position.x - obstacleXOffset,
                                                     closestObstaclePoint.position.y,
                                                     closestObstaclePoint.position.z);
                    Quaternion o_rotation = closestObstaclePoint.rotation;
                    GameObject gate = Instantiate(trackData.gatePrefab, g_position, g_rotation, transform);
                    GameObject obstacle = Instantiate(trackData.obstacleParent, o_position, o_rotation, transform);
                    GameObject pickup = Instantiate(trackData.pickupParent, p_position, o_rotation, transform);
                    gate.GetComponent<GatePropertyDistributor>().Distribute(trackData);
                    obstacle.GetComponent<ObstacleDataDistributor>().Distribute(trackData, assetLibrary);
                    pickup.GetComponent<PickupDataDistributor>().Distribute(trackData, assetLibrary);
                    previousGate = gate;
                }
               
            }
            else
            {
                Vector3 position = spawnedTrack.waypointsHolder.GetChild(i).position;
                Quaternion rotation = spawnedTrack.waypointsHolder.GetChild(i).rotation;
                GameObject gate = Instantiate(trackData.gatePrefab, new Vector3(position.z, position.y, position.z + UnityEngine.Random.Range(2, 5)), rotation, transform);
                gate.GetComponent<GatePropertyDistributor>().Distribute(trackData);
                previousGate = gate;
            }
            
        }

    }



    public void PrepareBeforeDistribute()
    {
#if UNITY_EDITOR
        assetLibrary.Clear();
        AssetDatabase.DeleteAsset(assetPath);
        AssetDatabase.Refresh();
        if (!AssetDatabase.IsValidFolder(assetPath))
        {
            AssetDatabase.CreateFolder("Assets", "Scriptable Object");
        }


        for (int i = 0; i < trackData.obstaclePrefab.Length; i++)
        {
            if (!assetLibrary.ContainsKey(trackData.obstaclePrefab[i]))
            {
                AssetDataSO obstacleSO = ScriptableObject.CreateInstance<AssetDataSO>();
                obstacleSO.name = trackData.obstaclePrefab[i].name;
                AssetDatabase.CreateAsset(obstacleSO, "Assets/Scriptable Object/" + obstacleSO.name + ".asset");
                AssetDatabase.SaveAssets();
                AssetDatabase.Refresh();
                AssetDataSO asset = (AssetDataSO)AssetDatabase.LoadAssetAtPath<AssetDataSO>("Assets/Scriptable Object/" + obstacleSO.name + ".asset");
                assetLibrary.Add(trackData.obstaclePrefab[i], asset);
                AssetDatabase.Refresh();
            }
        }
        for (int i = 0; i < trackData.pickupPrefab.Length; i++)
        {
            if (!assetLibrary.ContainsKey(trackData.pickupPrefab[i]))
            {
                AssetDataSO pickupSO = ScriptableObject.CreateInstance<AssetDataSO>();
                pickupSO.name = trackData.pickupPrefab[i].name;
                AssetDatabase.CreateAsset(pickupSO, "Assets/Scriptable Object/" + pickupSO.name + ".asset");
                AssetDatabase.SaveAssets();
                AssetDatabase.Refresh();
                AssetDataSO asset = (AssetDataSO)AssetDatabase.LoadAssetAtPath<AssetDataSO>("Assets/Scriptable Object/" + pickupSO.name + ".asset");
                assetLibrary.Add(trackData.pickupPrefab[i], asset);
                AssetDatabase.Refresh();
            }
        }
        GateDataSO gateDataSO = ScriptableObject.CreateInstance<GateDataSO>();
        gateDataSO.name = "Gate Data Repo";
        AssetDatabase.CreateAsset(gateDataSO, "Assets/Scriptable Object/" + gateDataSO.name + ".asset");
        AssetDatabase.SaveAssets();
        AssetDatabase.Refresh();
#endif
    }


    #endregion ALL SELF DECLEAR FUNCTIONS

}
