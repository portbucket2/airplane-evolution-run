using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimationOverrideController : APBehaviour
{

    private Animator animator;
    protected AnimatorOverrideController animatorOverrideController;
    protected TrackGenerator trackGenerator;

    #region ALL UNITY FUNCTIONS

    // Awake is called before Start
    public override void Awake()
    {
        base.Awake();
        trackGenerator = FindObjectOfType<TrackGenerator>();
    }

    // Start is called before the first frame update
    public override void Start()
    {
        base.Start();
        animator = transform.GetChild(0).GetComponent<Animator>();
        animatorOverrideController = new AnimatorOverrideController(animator.runtimeAnimatorController);
        animator.runtimeAnimatorController = animatorOverrideController;

        animatorOverrideController["Idle_1"] = trackGenerator.trackData.idleClip;
        animatorOverrideController["Slide"] = trackGenerator.trackData.gateInteractionClip;
        animatorOverrideController["VerticalHit"] = trackGenerator.trackData.pickupInteractionClip;
        animatorOverrideController["Melee Kick"] = trackGenerator.trackData.obstacleInteractionClip;
        animatorOverrideController["Run"] = trackGenerator.trackData.runClip;
    }

    void Update()
    {
        //if (gameState.Equals(GameState.GAME_INITIALIZED) && Input.GetMouseButtonDown(0))
        //{
        //    gameManager.ChangeGameState(GameState.GAME_PLAY_STARTED);
        //    gameState = GameState.GAME_PLAY_STARTED;
        //}

        if (!gameState.Equals(GameState.GAME_PLAY_STARTED))
            return;

    }

    void FixedUpdate()
    {
        if (!gameState.Equals(GameState.GAME_PLAY_STARTED))
            return;

    }

    void LateUpdate()
    {
        if (!gameState.Equals(GameState.GAME_PLAY_STARTED))
            return;

    }

    #endregion ALL UNITY FUNCTIONS
    //=================================   
    #region ALL OVERRIDING FUNCTIONS
    
    
    #endregion ALL OVERRIDING FUNCTIONS
    //=================================
    #region ALL SELF DECLEAR FUNCTIONS
    
    
    #endregion ALL SELF DECLEAR FUNCTIONS

}
