using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

[CreateAssetMenu(fileName = "New ObstacleData", menuName = "Obstacle Data", order = 51)]
public class AssetDataSO : ScriptableObject
{
    [SerializeField]
    private int value;
    [SerializeField]
    private GameObject objectPrefab;
    public static UnityAction<int> OnValidateAction;


    public int Value
    {
        get
        {
            return value;
        }

    }
    public GameObject Object
    {
        get
        {
            return objectPrefab;
        }
    }

    private void OnValidate()
    {
        OnValidateAction?.Invoke(value);
    }

}
