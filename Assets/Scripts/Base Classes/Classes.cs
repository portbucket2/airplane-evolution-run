using UnityEngine;
using System;
[Serializable]
public class TrackData 
{
    public GameObject trackMesh;
    [Header("Gate Attributes")]
    public GameObject gatePrefab;
    [HideInInspector]public int interval = 10;
    [Header("Interactable Attributes")]
    public GameObject obstacleParent;
    public GameObject[] obstaclePrefab;
    public GameObject pickupParent;
    public GameObject[] pickupPrefab;
    [Header("Date Limit Selection")]
    public int maxDayLimit;
    public int maxWeekLimit;
    public int maxMonthLimit;
    public int maxYearLimit;
    [Header("Animation Clips")]
    public AnimationClip idleClip;
    public AnimationClip runClip;
    public AnimationClip gateInteractionClip;
    public AnimationClip pickupInteractionClip;
    public AnimationClip obstacleInteractionClip;
    private bool showControlSelection = false;
    private bool showStraightPathControl = true;
    private bool showCurvedPathControl = false;
    private bool alreadyInstantiated = false;
    private GameObject spawnableGates;
    private uint regularObjectIntervalValue = 1;
    private bool showStrightControlSettings = false;
}
public class GateData
{
    public Color colorProperty;
    public GateValue gateValue;
    public Collider otherCollider;
}
public class ObstacleData
{
    public GameObject obstacle;
}
public class NumericData
{
    public Operator operation;
    public int value;
    public DateType dateType;
}
[System.Serializable]
public struct GateValue
{
    public Operator operatorType;
    public int value;
    public DateType dateType;
}

public class UpgradeSystemAvailability
{
    public bool isAgeUpgradeAvailable;
    public bool isIncomeUpgradeAvavilable;
}

public class UpgradeEntity
{
    public float rate;
    public int upgradeCost;
    public int levelCount;
    public bool availabilityStatus;

    public UpgradeEntity(float r, int c, int l, bool s)
    {
        this.rate = r;
        this.upgradeCost = c;
        this.levelCount = l;
        this.availabilityStatus = s;

    }
}