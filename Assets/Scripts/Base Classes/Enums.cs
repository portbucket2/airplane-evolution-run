public enum DateType
{
    DAY,
    WEEK,
    MONTH,
    YEAR
}
public enum Operator
{
    ADD,
    SUBTRACT,
    MULTIPLY,
    DIVIDE
}

