using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Obstacle : BaseInteractable<AssetDataSO>
{
    private int damage;
    #region ALL UNITY FUNCTIONS

    // Awake is called before Start
    public override void Awake()
    {
        base.Awake();
    }

    // Start is called before the first frame update
    public override void Start()
    {
        base.Start();
    }

    void Update()
    {
        //if (gameState.Equals(GameState.GAME_INITIALIZED) && Input.GetMouseButtonDown(0))
        //{
        //    gameManager.ChangeGameState(GameState.GAME_PLAY_STARTED);
        //    gameState = GameState.GAME_PLAY_STARTED;
        //}

        if (!gameState.Equals(GameState.GAME_PLAY_STARTED))
            return;

    }

    void FixedUpdate()
    {
        if (!gameState.Equals(GameState.GAME_PLAY_STARTED))
            return;

    }

    void LateUpdate()
    {
        if (!gameState.Equals(GameState.GAME_PLAY_STARTED))
            return;

    }

    #endregion ALL UNITY FUNCTIONS
    //=================================   
    #region ALL OVERRIDING FUNCTIONS
    public override void OnGameInitializing()
    {
        AssetDataSO.OnValidateAction += ValueUpdate;
    }
    public override void OnDisable()
    {
        AssetDataSO.OnValidateAction -= ValueUpdate;
    }
    private void ValueUpdate(int arg0)
    {
        damage = arg0;
    }

    public override void Initialize(AssetDataSO data)
    {
        damage = data.Value;
    }
    public override void Execute()
    {
        base.Execute();
        gameObject.SetActive(false);
        CurrencyDataManager.Instance.ReductAge(damage);

    }
    #endregion ALL OVERRIDING FUNCTIONS
    //=================================
    #region ALL SELF DECLEAR FUNCTIONS


    #endregion ALL SELF DECLEAR FUNCTIONS

}
