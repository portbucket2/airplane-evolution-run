using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;


public class MeshFabricator : APBehaviour
{
    [Header("Data")]
    public int age;

    [Serializable]
    public class AgeActor : IComparable<AgeActor> 
    {
        [field: SerializeField]
        public string Name { get; set; }
        [field: SerializeField]
        public GameObject MeshObject { get; set; }
        [field: SerializeField]
        public int StartingAge { get; set; }
        [field: SerializeField]
        public int Speed { get; set; }

        public int CompareTo(AgeActor other)
        {
            if (other == null)
                return 1;
            return StartingAge - other.StartingAge;

        }
    }
    [Header("Current Character Data")]
    public AgeActor currentCharacter;
    [SerializeField]
    private AgeActor upperBoundOfCurrentCharacter;
    [SerializeField]
    private AgeActor lowerBoundOfCurrentCharacter;
    //public GenericDictionary<int, AgeActor> actorCollection = new GenericDictionary<int, AgeActor>();
    public List<AgeActor> actorList = new List<AgeActor>();
    #region ALL UNITY FUNCTIONS

    // Awake is called before Start
    public override void Awake()
    {
        base.Awake();
    }

    // Start is called before the first frame update
    public override void Start()
    {
        base.Start();
    }

    void Update()
    {
        //if (gameState.Equals(GameState.GAME_INITIALIZED) && Input.GetMouseButtonDown(0))
        //{
        //    gameManager.ChangeGameState(GameState.GAME_PLAY_STARTED);
        //    gameState = GameState.GAME_PLAY_STARTED;
        //}

        if (!gameState.Equals(GameState.GAME_PLAY_STARTED))
            return;

    }

    void FixedUpdate()
    {
        if (!gameState.Equals(GameState.GAME_PLAY_STARTED))
            return;

    }

    void LateUpdate()
    {
        if (!gameState.Equals(GameState.GAME_PLAY_STARTED))
            return;

    }

    #endregion ALL UNITY FUNCTIONS
    //=================================   
    #region ALL OVERRIDING FUNCTIONS
    public override void OnGameInitializing()
    {
        age = CurrencyDataManager.Instance.Age.Year;
        MeshCollectionSetup();
        currentCharacter = GetMeshAtAge(age);
        ActivateMesh(currentCharacter);
        upperBoundOfCurrentCharacter = GetUpperBoundCharacter(currentCharacter);
        lowerBoundOfCurrentCharacter = GetLowerBoundCharacter(currentCharacter);
        CurrencyDataManager.Instance.OnAgeChange += AgeUpdatedCallback;
    }

    public override void OnGameOver()
    {
        CurrencyDataManager.Instance.OnAgeChange -= AgeUpdatedCallback;
    }

    public override void OnGameStart()
    {
        
    }

    #endregion ALL OVERRIDING FUNCTIONS
    //=================================
    #region ALL SELF DECLEAR FUNCTIONS
    private void AgeUpdatedCallback(int age)
    {
        this.age = age;
        if(upperBoundOfCurrentCharacter != null)
        {
            if(age >= upperBoundOfCurrentCharacter.StartingAge)
            {
                Debug.Log("<color=green> Has Upper Character </color>");
                DeactivateMesh(currentCharacter);
                currentCharacter = GetMeshAtAge(age);
                ActivateMesh(currentCharacter);
                upperBoundOfCurrentCharacter = GetUpperBoundCharacter(currentCharacter);
                lowerBoundOfCurrentCharacter = GetLowerBoundCharacter(currentCharacter);
            }
        }
        if(age < currentCharacter.StartingAge)
        {
            if(lowerBoundOfCurrentCharacter != null)
            {
                Debug.Log("<color=red> Has lower Character </color>");
                DeactivateMesh(currentCharacter);
                currentCharacter = GetLowerBoundCharacter(currentCharacter);
                ActivateMesh(currentCharacter);
                upperBoundOfCurrentCharacter = GetUpperBoundCharacter(currentCharacter);
                lowerBoundOfCurrentCharacter = GetLowerBoundCharacter(currentCharacter);
            }
            
        }
    }
    private void MeshCollectionSetup()
    {
        for(int i = 0; i < transform.childCount; i++)
        {
            Transform obj = transform.GetChild(i);
            actorList.Add(new AgeActor()
            {
                Name = obj.name,
                MeshObject = obj.gameObject,
                StartingAge = age + i * 5,
                Speed = i * 2 + 5
            });
        }
    }
    public AgeActor GetMeshAtAge(int age)
    {
        if (actorList.Count == 0)
            Debug.Log("<color=red> Mesh Missing </color>");
        if (actorList.Count == 1)
            return actorList[0];
        AgeActor lowerBoundCharacter = actorList[0];
        AgeActor upperBoundCharacter = actorList[1];
        for(int i = 0; i < actorList.Count -1; i++)
        {
            if (age >= lowerBoundCharacter.StartingAge && age < upperBoundCharacter.StartingAge)
            {
                return lowerBoundCharacter;
            }
            else
            {
                lowerBoundCharacter = upperBoundCharacter;
                if (actorList.IndexOf(upperBoundCharacter) < actorList.Count - 1)
                {
                    upperBoundCharacter = actorList[actorList.IndexOf(upperBoundCharacter) + 1];
                }
                else
                {
                    break;
                }
            }
        }
        return actorList[actorList.Count - 1];  
    }
    public void ActivateMesh(AgeActor actor)
    {
        actor.MeshObject.SetActive(true);
    }
    public void DeactivateMesh(AgeActor actor)
    {
        actor.MeshObject.SetActive(false);
    }
    public AgeActor GetUpperBoundCharacter(AgeActor currentCharacter)
    {
        int indexOfCurrent = actorList.IndexOf(currentCharacter);
        if(indexOfCurrent < actorList.Count - 1)
        {
            return actorList[indexOfCurrent + 1];
        }
        else
        {
            return null;
        }
        
    }
    public AgeActor GetLowerBoundCharacter(AgeActor currentCharacter)
    {
        int indexOfCurrent = actorList.IndexOf(currentCharacter);
        if (indexOfCurrent > 0)
        {
            return actorList[indexOfCurrent - 1];
        }
        else
            return null;
    }
    #endregion ALL SELF DECLEAR FUNCTIONS

}
