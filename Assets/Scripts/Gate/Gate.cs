using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

#if UNITY_EDITOR
using UnityEditor;
#endif
using System;

public class Gate : BaseInteractable<GateData>
{
    public Renderer selfRenderer;
    public Color color;
    public TextMeshProUGUI text;
    public Material material;
    public GateDataSO asset;
    public Collider siblingCollider;
    public GateValue selfData;
    public int instanceID;
#region ALL UNITY FUNCTIONS

    // Awake is called before Start
    public override void Awake()
    {
        base.Awake();
    }

    // Start is called before the first frame update
    public override void Start()
    {
        base.Start();
    }

    void Update()
    {
        //if (gameState.Equals(GameState.GAME_INITIALIZED) && Input.GetMouseButtonDown(0))
        //{
        //    gameManager.ChangeGameState(GameState.GAME_PLAY_STARTED);
        //    gameState = GameState.GAME_PLAY_STARTED;
        //}

        if (!gameState.Equals(GameState.GAME_PLAY_STARTED))
            return;

    }

    void FixedUpdate()
    {
        if (!gameState.Equals(GameState.GAME_PLAY_STARTED))
            return;

    }

    void LateUpdate()
    {
        if (!gameState.Equals(GameState.GAME_PLAY_STARTED))
            return;

    }

#endregion ALL UNITY FUNCTIONS
    //=================================   
#region ALL OVERRIDING FUNCTIONS
    public override void OnGameInitializing()
    {
        base.OnGameInitializing();
        selfRenderer = GetComponent<Renderer>();
        //EditorApplication.playModeStateChanged += Reassign;
    }
    //private void Reassign(PlayModeStateChange obj)
    //{
    //    if (obj.Equals(PlayModeStateChange.ExitingPlayMode))
    //    {          
    //        GateDataSO asset = AssetDatabase.LoadAssetAtPath<GateDataSO>("Assets/Scriptable Object/Gate Data Repo.asset");
    //        asset.OnValidateAction += ApplyChanges;
    //    }
    //}

    public override void OnDisable()
    {
        base.OnDisable();
    }
    public override void Initialize(GateData gateData)
    {
        base.Initialize(gateData);
        asset = Resources.Load<GateDataSO>("RunnerEngine/Gate Data Repo");
        //asset = AssetDatabase.LoadAssetAtPath<GateDataSO>("Assets/Scriptable Object/Gate Data Repo.asset");
        instanceID = gameObject.GetHashCode();
        asset.GateValueDictionary.Add(instanceID, gateData.gateValue);
        Material materialInstance = new Material(material.shader);
        materialInstance.SetColor("_Color", gateData.colorProperty);
        selfRenderer.sharedMaterial = materialInstance;
        color = gateData.colorProperty;
        text.text =gateData.gateValue.operatorType.ToString() + " " +
                               gateData.gateValue.value.ToString() + " " +
                               gateData.gateValue.dateType.ToString();
        siblingCollider = gateData.otherCollider;
        asset.OnValidateAction += ApplyChanges;
    }

    private void ApplyChanges(GenericDictionary<int, GateValue> arg0)
    {

        if (arg0.TryGetValue(gameObject.GetHashCode(), out selfData))
        {
            this.text.text = selfData.operatorType.ToString() + " " +
                        selfData.value.ToString() + " " +
                        selfData.dateType.ToString();

        }
        else
        {
            Debug.Log("Missing Reference");
        }
    }

    public override void Execute()
    {
        base.Execute();
        siblingCollider.enabled = false;
        gameObject.SetActive(false);
        GateDataSO asset = Resources.Load<GateDataSO>("RunnerEngine/Gate Data Repo");
        //GateDataSO asset = AssetDatabase.LoadAssetAtPath<GateDataSO>("Assets/Scriptable Object/Gate Data Repo.asset");
        if (asset.GateValueDictionary.TryGetValue(instanceID, out selfData))
        {
            CurrencyDataManager.Instance.ApplyOperator(selfData);
            
        }
        
    }

#endregion ALL OVERRIDING FUNCTIONS
    //=================================
#region ALL SELF DECLEAR FUNCTIONS


#endregion ALL SELF DECLEAR FUNCTIONS

}
