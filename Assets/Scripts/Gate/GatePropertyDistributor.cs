using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using System;
using Random = UnityEngine.Random;

public class GatePropertyDistributor : APBehaviour
{
    [Header("Color Properties")]
    public Color colorPositive;
    public Color colorNegative;

    [Header("Gate Properties")]
    public Gate leftGate;
    public Gate rightGate;

    private DateType tempDateType;
    #region ALL UNITY FUNCTIONS

    // Awake is called before Start
    public override void Awake()
    {
        base.Awake();
    }

    // Start is called before the first frame update
    public override void Start()
    {
        base.Start();
    }

    void Update()
    {
        //if (gameState.Equals(GameState.GAME_INITIALIZED) && Input.GetMouseButtonDown(0))
        //{
        //    gameManager.ChangeGameState(GameState.GAME_PLAY_STARTED);
        //    gameState = GameState.GAME_PLAY_STARTED;
        //}

        if (!gameState.Equals(GameState.GAME_PLAY_STARTED))
            return;

    }

    void FixedUpdate()
    {
        if (!gameState.Equals(GameState.GAME_PLAY_STARTED))
            return;

    }

    void LateUpdate()
    {
        if (!gameState.Equals(GameState.GAME_PLAY_STARTED))
            return;

    }

    #endregion ALL UNITY FUNCTIONS
    //=================================   
    #region ALL OVERRIDING FUNCTIONS


    #endregion ALL OVERRIDING FUNCTIONS
    //=================================
    #region ALL SELF DECLEAR FUNCTIONS
    public DateType GetRandomEnumValues(int iterationNumber)
    {
        System.Random random = new System.Random();
        DateType value = DateType.YEAR;
        Type type = typeof(DateType);
        Array values = type.GetEnumValues();
        for(int i = 0; i < iterationNumber; ++i)
        {
            int index = random.Next(values.Length);

            value = (DateType)values.GetValue(index);
            
        }
        //Debug.Log("Enum " + value);
        return value;
    }

    
    private int RandomDatetime(TrackData trackData, DateType dateType)
    {
        int range = 0;
        switch (dateType)
        {
            case DateType.YEAR:
                range = UnityEngine.Random.Range(1, trackData.maxYearLimit);
                return range;
            case DateType.MONTH:
                range = UnityEngine.Random.Range(1, trackData.maxMonthLimit);
                return range;
            case DateType.WEEK:
                range = UnityEngine.Random.Range(1, trackData.maxWeekLimit);
                return range;
            case DateType.DAY:
                range = UnityEngine.Random.Range(1, trackData.maxDayLimit);
                return range;
        }
        return range;
    }
    private Operator GetIncrementalOperator(bool isIncremental, DateType dateType)
    {
        System.Random rnd = new System.Random();
        int randomChance = rnd.Next(100);
        switch (isIncremental)
        {
            //case true:
            //    if (randomChance >= 50 && dateType != DateType.YEAR) return Operator.ADD;
            //    else return Operator.MULTIPLY;
            //case false:
            //    if (randomChance >= 50 && dateType != DateType.YEAR) return Operator.SUBTRACT;
            //    else return Operator.DIVIDE;
            case true:
                return Operator.ADD;
            case false:
                return Operator.SUBTRACT;
        }
    }
    public void Distribute(TrackData trackData)
    {
        bool isRightSidePositive = Random.Range(0, 2) == 0;
        GateData leftGateData = new GateData();
        GateData rightGateData = new GateData();
        if (isRightSidePositive)
        {
            leftGateData.colorProperty = colorNegative;
            //Debug.Log(GetIncrementalOperator(true));
            
            tempDateType = GetRandomEnumValues(10);
            leftGateData.gateValue.dateType = tempDateType;
            leftGateData.gateValue.operatorType = GetIncrementalOperator(false, tempDateType);
            leftGateData.gateValue.value = RandomDatetime(trackData, tempDateType);
            rightGateData.colorProperty = colorPositive;
            
            tempDateType = GetRandomEnumValues(12);
            rightGateData.gateValue.dateType = tempDateType;
            rightGateData.gateValue.operatorType = GetIncrementalOperator(true, tempDateType);
            rightGateData.gateValue.value = RandomDatetime(trackData, tempDateType);

        }
        else
        {
            rightGateData.colorProperty = colorNegative;
            
            tempDateType = GetRandomEnumValues(8);
            rightGateData.gateValue.dateType = tempDateType;
            rightGateData.gateValue.operatorType = GetIncrementalOperator(false, tempDateType);
            rightGateData.gateValue.value = RandomDatetime(trackData, tempDateType);
            leftGateData.colorProperty = colorPositive;
            
            tempDateType = GetRandomEnumValues(15);
            leftGateData.gateValue.dateType = tempDateType;
            leftGateData.gateValue.operatorType = GetIncrementalOperator(true, tempDateType);
            leftGateData.gateValue.value = RandomDatetime(trackData, tempDateType);
        }
        rightGateData.otherCollider = leftGate.GetComponent<Collider>();
        leftGateData.otherCollider = rightGate.GetComponent<Collider>();
        leftGate.Initialize(leftGateData);
        rightGate.Initialize(rightGateData);
    }
    #endregion ALL SELF DECLEAR FUNCTIONS

}
