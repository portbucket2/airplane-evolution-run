using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEditor;

[ExecuteAlways]
public class GateDataSO : ScriptableObject
{
    public GenericDictionary<int, GateValue> gateDataDict = new GenericDictionary<int, GateValue>();
    [SerializeReference]
    public UnityAction<GenericDictionary<int, GateValue>> OnValidateAction;
    public UnityAction<GenericDictionary<int, GateValue>> OnManualUpdate;


    public GenericDictionary<int, GateValue> GateValueDictionary
    {
        get
        {
            return gateDataDict;
        }

    }
    public void OnValidate()
    {
        Debug.Log("Validating");
        OnValidateAction?.Invoke(GateValueDictionary);
    }
    public void ManualOverride()
    {       
        OnManualUpdate?.Invoke(GateValueDictionary);
    }
}
