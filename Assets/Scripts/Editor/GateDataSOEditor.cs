using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(GateDataSO))]
public class GateDataSOEditor : Editor
{
    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();
        var scriptReference = (GateDataSO)target;
        if(GUILayout.Button("Update", GUILayout.Height(40)))
        {
            scriptReference.ManualOverride();
        }
    }
}
