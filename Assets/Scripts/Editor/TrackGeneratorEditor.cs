#if UNITY_EDITOR
using UnityEditor;
using UnityEngine;
using Com.AlphaPotato.Utility;

[CustomEditor(typeof(TrackGenerator))]
public class TrackGeneratorEditor : APEditor
{
    private TrackGenerator scriptReference;

    // All SerializedProperties
    #region ALL_PUBLIC_PROPERTIES
    private SerializedProperty trackData;
    private SerializedProperty playerPrefab;
    private SerializedProperty distanceDelta;

    #endregion ALL_PUBLIC_PROPERTIES

    bool drawProperties = true;
    public void OnEnable()
    {
        scriptReference = (TrackGenerator)target;
        #region FINDER_ALL_PUBLIC_PROPERTIES_FINDER
        trackData = serializedObject.FindProperty("trackData");
        playerPrefab = serializedObject.FindProperty("playerPrefab");
        distanceDelta = serializedObject.FindProperty("distanceDelta");


        #endregion FINDER_ALL_PUBLIC_PROPERTIES
    }

    public override void OnInspectorGUI()
    {
        serializedObject.Update();

        #region DrawProperty(propertyName)
        DrawProperty(trackData);
        DrawProperty(playerPrefab);
        DrawProperty(distanceDelta);

        #endregion DrawProperty(propertyName)

        Space();
        DrawHorizontalLine();
        OnButtonPressed("Generate Track",
            () =>
            {
                scriptReference.GenerateTrack();
            },
            new EditorButtonStyle
            {
                buttonColor = Color.green,
                buttonTextColor = Color.white
            });

        serializedObject.ApplyModifiedProperties();
    }
}
#endif
