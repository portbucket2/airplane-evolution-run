using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System;

public static class PlayStateModifier 
{
    static PlayStateModifier()
    {
        EditorApplication.playModeStateChanged += ModeChanged;
    }

    private static void ModeChanged(PlayModeStateChange obj)
    {
        if(obj.Equals(PlayModeStateChange.ExitingPlayMode))
        {

        }
    }
}
