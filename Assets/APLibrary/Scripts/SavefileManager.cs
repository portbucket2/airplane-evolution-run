﻿/*
 * Developer Name: Md. Imran Hossain
 * E-mail: sandsoftimer@gmail.com
 * FB: https://www.facebook.com/md.imran.hossain.902
 * in: https://www.linkedin.com/in/md-imran-hossain-69768826/
 * 
 * Features: 
 * Saving gameplay data
 * Loading gameplay data  
 */

using UnityEngine;

namespace Com.AlphaPotato.Utility
{
    public class SavefileManager : MonoBehaviour
    {
        public void SaveGameData(GameplayData gameplayData)
        {
            PlayerPrefsX.SetBool(ConstantManager.isGameoverSuccess, gameplayData.isGameoverSuccess);

            PlayerPrefs.SetInt(ConstantManager.gameScore, gameplayData.gameScore);
            PlayerPrefs.SetInt(ConstantManager.ageUpgradeLevel, gameplayData.ageUpgradeLevel);
            PlayerPrefs.SetInt(ConstantManager.incomeUpgradeLevel, gameplayData.incomeUpgradeLevel);
            PlayerPrefs.SetInt(ConstantManager.currentLevelNumber, gameplayData.currentLevelNumber);
            PlayerPrefs.SetInt(ConstantManager.ageUpgradeCost, gameplayData.ageUpgradeCost);
            PlayerPrefs.SetInt(ConstantManager.incomeUpgradeCost, gameplayData.incomeUpgradeCost);

            PlayerPrefs.SetFloat(ConstantManager.gameStartTime, gameplayData.gameStartTime);
            PlayerPrefs.SetFloat(ConstantManager.upgradeRate, gameplayData.upgradeRate);
            PlayerPrefs.SetFloat(ConstantManager.gameEndTime, gameplayData.gameEndTime);
            PlayerPrefs.SetFloat(ConstantManager.totalLevelCompletedTime, gameplayData.totalLevelCompletedTime);
        }

        public GameplayData LoadGameData()
        {
            GameplayData gameplayData = new GameplayData();

            gameplayData.isGameoverSuccess = PlayerPrefsX.GetBool(ConstantManager.isGameoverSuccess, false);

            gameplayData.gameScore = PlayerPrefs.GetInt(ConstantManager.gameScore, 500);
            gameplayData.ageUpgradeLevel = PlayerPrefs.GetInt(ConstantManager.ageUpgradeLevel, 1);
            gameplayData.incomeUpgradeLevel = PlayerPrefs.GetInt(ConstantManager.incomeUpgradeLevel, 1);
            gameplayData.ageUpgradeCost = PlayerPrefs.GetInt(ConstantManager.ageUpgradeCost, 150);
            gameplayData.incomeUpgradeCost = PlayerPrefs.GetInt(ConstantManager.incomeUpgradeCost, 250);
            gameplayData.currentLevelNumber = PlayerPrefs.GetInt(ConstantManager.currentLevelNumber, 0);

            gameplayData.upgradeRate = PlayerPrefs.GetFloat(ConstantManager.upgradeRate, 0.5f);
            gameplayData.gameStartTime = PlayerPrefs.GetFloat(ConstantManager.gameStartTime, 0f);
            gameplayData.gameEndTime = PlayerPrefs.GetFloat(ConstantManager.gameEndTime, 0f);
            gameplayData.totalLevelCompletedTime = PlayerPrefs.GetFloat(ConstantManager.totalLevelCompletedTime, 0f);

            return gameplayData;
        }
    }
}