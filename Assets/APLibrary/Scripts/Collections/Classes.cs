﻿using System;
using UnityEngine;

[Serializable]
public class GameplayData
{
    public int gameScore;
    public int ageUpgradeLevel;
    public int incomeUpgradeLevel;
    public int currentLevelNumber;
    public float upgradeRate;
    public int incomeUpgradeCost;
    public int ageUpgradeCost;

    public float gameStartTime, gameEndTime;
    public float totalLevelCompletedTime;

    public bool isGameoverSuccess;

    public GameplayData()
    {
        gameScore = 500;
        ageUpgradeLevel = 1;
        incomeUpgradeLevel = 1;
        currentLevelNumber = 0;
        upgradeRate = 0.5f;
        incomeUpgradeCost = 250;
        ageUpgradeCost = 150;
        gameStartTime = 0f;
        gameEndTime = 0f;
        totalLevelCompletedTime = 0f;

        isGameoverSuccess = false;
    }
}

public class EditorButtonStyle
{
    public Color buttonColor;
    public Color buttonTextColor;
}